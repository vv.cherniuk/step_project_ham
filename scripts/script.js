//Section - 3 tabs
const serviceUl = document.querySelector(".service-section-nav-wrapper ul");
const serviceLi = Array.from(
  document.querySelectorAll(".service-section-nav-wrapper li")
);
const serviceDiv = Array.from(document.querySelectorAll(".service-menu-item"));

serviceUl.addEventListener("click", (event) => {
  serviceLi.forEach((el, index) => {
    el.classList.remove("list-active");
    el.dataset.li = index;
  });
  event.target.classList.add("list-active");

  serviceDiv.forEach((elem, index) => {
    elem.dataset.li = index;
    elem.classList.remove("active");
    elem.classList.add("not-active");
  });
  serviceDiv.forEach((elem) => {
    if (elem.dataset.li === event.target.dataset.li) {
      elem.classList.add("active");
    }
  });
});

//Gallery Amazing work
const amazingWorkUl = document.querySelector(".section-5 ul");
const amazingWorkLi = Array.from(document.querySelectorAll(".section-5 li"));
const image = Array.from(document.querySelectorAll(".image-gallery img"));
const buttonLoad = document.querySelector(".section-5 button");
let counter = 3;

amazingWorkUl.addEventListener("click", (event) => {
  if (document.querySelector(".section-5 button").style.display === "none") {
    buttonLoad.style.display = "";
  }
  amazingWorkLi.forEach((el) => {
    el.classList.remove("menu-active");
  });
  event.target.classList.add("menu-active");

  image.forEach((element) => {
    element.classList.add("image-not-active");
    if (event.target.getAttribute("name") === element.getAttribute("name")) {
      element.classList.remove("image-not-active");
      buttonLoad.style.display = "none";
    } else if (event.target.getAttribute("name") === "all") {
      if (counter > 6) {
        buttonLoad.style.display = "none";
      }
      image.forEach((elem) => {
        if (
          Number(elem.dataset.number) <= counter &&
          document.querySelector(".section-5 li:first-child").className ===
            "menu-active"
        ) {
          elem.classList.remove("image-not-active");
        }
      });
    }
  });
});

/*Load Button*/
buttonLoad.addEventListener("click", () => {
  document.querySelector(".preloader").classList.remove("preloader-hide");
  setTimeout(function () {
    document.querySelector(".preloader").classList.add("preloader-hide");
  }, 1000);
  setTimeout(loadGallery, 1000);
});

function loadGallery() {
  counter += 3;
  image.forEach((el) => {
    if (
      Number(el.dataset.number) <= counter &&
      document.querySelector(".section-5 li:first-child").className ===
        "menu-active"
    ) {
      el.classList.remove("image-not-active");
    }
  });
  if (counter > 6) {
    buttonLoad.style.display = "none";
  }
}

/*Section-7 People Say*/
const ulPeopleSay = document.querySelector(".section-7 ul");
const liItemImg = Array.from(document.querySelectorAll(".section-7 li img"));
const articleItem = Array.from(
  document.querySelectorAll(".about-section-wrapper")
);
const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

liItemImg.forEach((el, index) => {
  el.parentElement.dataset.index = index;
});
articleItem.forEach((elem, index) => {
  elem.dataset.index = index;
});

liItemImg.forEach((element) => {
  element.addEventListener("click", (event) => {
    liItemImg.forEach((el) => {
      el.parentElement.classList.remove("active-li");
    });
    event.target.parentElement.classList.add("active-li");
    articleItem.forEach((elem) => {
      elem.classList.add("about-hide");
    });
    articleItem.forEach((elem) => {
      if (elem.dataset.index === event.target.parentElement.dataset.index) {
        elem.classList.remove("about-hide");
      }
    });
  });
});

leftArrow.addEventListener("click", moveLeft);
rightArrow.addEventListener("click", moveRight);

function moveLeft() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === 0) {
      count = liItemImg.length;
    }
    if (Number(el.parentElement.dataset.index) === count - 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count - 1) {
      el.classList.remove("about-hide");
    }
  });
}

function moveRight() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === liItemImg.length - 1) {
      count = -1;
    }
    if (Number(el.parentElement.dataset.index) === count + 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count + 1) {
      el.classList.remove("about-hide");
    }
  });
}

/*Best Images Gallery*/
const bestImagesButton = document.querySelector(".section-8 button");
bestImagesButton.addEventListener("click", () => {
  document.querySelector(".preloader-2").classList.remove("preloader-hide");
  setTimeout(function () {
    document.querySelector(".preloader-2").classList.add("preloader-hide");
  }, 1000);
  setTimeout(addImages, 1000);
});

//Masonry
function masonryLoad() {
  const gallery = document.querySelector(".grid");
  new Masonry(gallery, {
    itemSelector: ".grid-item",
    transitionDuration: "0.1s",
    fitWidth: true,
    stagger: 10,
  });
}
masonryLoad();

//addImages function
function addImages() {
  const imgBest = Array.from(document.querySelectorAll(".grid img"));
  imgBest.forEach((element) => {
    element.classList.remove("image-hided");
    masonryLoad();
    bestImagesButton.remove();
  });
}
